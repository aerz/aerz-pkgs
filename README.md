# Aerz's AUR Packages

# Pacman package repository

Import GPG key:

```sh
pacman-key --recv-keys ED300246E4E5D0BE --keyserver keyserver.ubuntu.com
pacman-key --lsign-key ED300246E4E5D0BE
```

Add the repository in `/etc/pacman.conf`:

```ini
[aerz-pkgs]
Server = https://aerz.gitlab.io/$repo/$arch
```

Refresh all package databases:

```sh
sudo pacman -Syy
```

# Packages available

- [brave](https://brave.com/) - A [personal patched](https://github.com/aerz/brave-patches) version
